package com.example.thinhnv.charge1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private ImageView ivPreview;
    private Uri imageUri;
    private static final int MEDIA_TYPE_IMAGE=1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE=100;
    private static final String DIRECTORY_IMAGE="charge";
    public static final String IMAGE_URI="IMAGE_URI";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ivPreview=(ImageView) findViewById(R.id.ivPreview);
        // GET DATA FROM SHARED_PREFERENCE
        SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(this);
        boolean isLogin=sharedPref.getBoolean(LoginActivity.IS_LOGIN, false);
        String mobileNetwork=sharedPref.getString(LoginActivity.MOBILE_NETWORK, "Not login");
        if (!isLogin){
            startLogin();
        }
        else {
            Toast.makeText(MainActivity.this, sharedPref.getString(IMAGE_URI, "null image"), Toast.LENGTH_SHORT).show();
            captureImage();

        }

        Toast.makeText(MainActivity.this, "get "+isLogin+" "+mobileNetwork, Toast.LENGTH_SHORT).show();
    }
    // SET LOGIN SCREEN
    private void startLogin() {
        Intent intentLoginScreen=new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intentLoginScreen);
    }

    // SET FUNCTION CAPTURE IMAGE
    private void captureImage() {
        Intent intentCaptureImage=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri=getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intentCaptureImage.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intentCaptureImage, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==CAMERA_CAPTURE_IMAGE_REQUEST_CODE && resultCode==RESULT_OK) {
            previewCaptureImage();
        }
    }
    // SET FUNCTION PREVIEW CAPTURE IMAGE
    private void previewCaptureImage() {
        ivPreview.setVisibility(View.VISIBLE);
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inSampleSize=1;
        final Bitmap bitmap=BitmapFactory.decodeFile(imageUri.getPath(), options);
        ivPreview.setImageBitmap(bitmap);
        SharedPreferences sharedPref=PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor=sharedPref.edit();
        editor.putString(IMAGE_URI, imageUri.toString());
        editor.commit();


        // image processing
        String digitalNumber=imageProcessing(imageUri);
        // send ussd code
        sendUSSD_CODE(digitalNumber);

    }

    // FUNCTION NOT OK
    // SET FUNCTION IMAGE PROCESSING
    private String imageProcessing(Uri fileUri){
        return ("01234567890123");
    }
    // SET FUNCTION SEND USSD CODE
    private void sendUSSD_CODE(String digitalNumber){
        String ussd="*101"+Uri.encode("#");
        Intent i=new Intent("android.intent.action.CALL", Uri.parse("tel:"+ussd));
        startActivity(i);
    }




    //***************************************************/
    //default any code
    //**************************************************/
    // CREATE FILE URI TO STORE IMAGE
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }
    // RETURN IMAGE FILE
    private static File getOutputMediaFile(int type){
        File mediaStoreDir=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DIRECTORY_IMAGE);
        if (!mediaStoreDir.exists()) {
            if (!mediaStoreDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmSS", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type==MEDIA_TYPE_IMAGE){
            mediaFile=new File(mediaStoreDir.getPath()+File.separator+"IMG_"+timeStamp+".jpg");
        }
        else  return null;
        return mediaFile;
    }
}
