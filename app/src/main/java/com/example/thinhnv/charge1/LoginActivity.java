package com.example.thinhnv.charge1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.thinhnv.charge1.R.*;

public class LoginActivity extends AppCompatActivity {
    private Spinner spinMN;
    private Button btStartCharge1;
    public static final String MOBILE_NETWORK="MOBILE_NETWORK";
    public static final String IS_LOGIN="IS_LOGIN";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_login);
        spinMN=(Spinner) findViewById(id.spinnerMobileNetwork);
        btStartCharge1=(Button) findViewById(id.btStartCharge1);
        final String[] listMobileNetWork=getResources().getStringArray(array.list_mobile_network);
        
        
        // SET ADAPTER FOR SPINNER
        ArrayAdapter<CharSequence> adapterSpinMN=ArrayAdapter.createFromResource(this,
                array.list_mobile_network, android.R.layout.simple_spinner_item);
        spinMN.setAdapter(adapterSpinMN);
        spinMN.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences sharedPref=PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                SharedPreferences.Editor editor=sharedPref.edit();
                editor.putString(MOBILE_NETWORK, listMobileNetWork[position]);
                editor.putBoolean(IS_LOGIN, true);
                editor.commit();
                Toast.makeText(LoginActivity.this, "Save "+listMobileNetWork[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                
            }
        });

        // SET CLICK CHARGE 1
        btStartCharge1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
        });

    }
}
